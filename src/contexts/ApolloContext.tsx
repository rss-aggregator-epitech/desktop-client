import { createUploadLink } from 'apollo-upload-client'
import React, { createContext, useContext, useEffect, useState } from 'react'

import { ApolloClient, ApolloProvider, InMemoryCache, NormalizedCacheObject } from '@apollo/client'
import { setContext } from '@apollo/client/link/context'

const TOKEN_KEY = 'token'

const uri = 'https://rss-aggregator-server-prod.herokuapp.com/admin/api'
const httpLink = createUploadLink({ uri, credentials: 'include' })

type ApolloContextType = {
  client: ApolloClient<NormalizedCacheObject>
  setToken: (token: string)=> void
  removeToken: ()=> void
  token: string | null
  isSignedIn: boolean
}

// @ts-ignore
const ApolloContext = createContext<ApolloContextType>({})

export const ApolloContextProvider: React.FC = ({ children }) => {
  const [token, setToken] = useState<string | null>(localStorage.getItem(TOKEN_KEY))

  useEffect(() => {
    if (token)
      localStorage.setItem(TOKEN_KEY, token)
    else
      localStorage.removeItem(TOKEN_KEY)
  }, [token])

  const removeToken = () => {
    setToken(null)
  }

  const authLink = setContext((_, { headers }) => {
    // get the authentication token from local storage if it exists
    const token = localStorage.getItem('token')
    // return the headers to the context so httpLink can read them
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : '',
      },
    }
  })

  const client = new ApolloClient({
    cache: new InMemoryCache(),
    link: authLink.concat(httpLink),
    headers: {
      authorization: token ? `Bearer ${token}` : '',
    },
  })

  return (
    <ApolloContext.Provider value={{
      client,
      setToken,
      removeToken,
      token,
      isSignedIn: !!token,
    }}
    >
      <ApolloProvider client={client}>
        {children}
      </ApolloProvider>
    </ApolloContext.Provider>
  )
}

export const useApollo = () => useContext(ApolloContext)
