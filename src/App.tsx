
import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import { createMuiTheme, CssBaseline, ThemeProvider } from '@material-ui/core'

import { Navbar } from './components/Navbar'
import ProtectedRoute from './components/ProtectedRoute'
import ProtectedRouteNC from './components/ProtectedRouteNC'
import { ApolloContextProvider } from './contexts/ApolloContext'
import Dashboard from './pages/Dashboard'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'

const theme = createMuiTheme({
  palette: {
    text: {
      primary: '#023e8a',
      secondary: '#03045e',
    },
    background: {
      default: '#696969',
    },
    primary: {
      main: '#009387',
    },
    secondary: {
      main: '#ffffff',
    },
    info: {
      main: '#ff79c6',
    },
    error: {
      main: '#ff79c6',
    },

  },
  typography: {
    allVariants: {
      color: '#48bfe3',
    },
  },
})

function App() {
  return (
    <ApolloContextProvider>
      <BrowserRouter>
        <ThemeProvider theme={theme}>
          <Navbar />
          <Switch>
            <ProtectedRouteNC exact path="/" component={Home}/>
            <ProtectedRouteNC exact path="/login" component={Login}/>
            <ProtectedRouteNC exact path="/register" component={Register}/>
            <ProtectedRoute exact path="/dashboard" component={Dashboard} />
          </Switch>
          <CssBaseline/>
        </ThemeProvider>
      </BrowserRouter>
    </ApolloContextProvider>
  )
}

export default App
