import { AppBar, Container, Typography, Button } from '@material-ui/core'
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles'
import { useApollo } from '../../contexts/ApolloContext'
import RssFeedIcon from '@material-ui/icons/RssFeed'
import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'

interface NavbarProps {
}

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    display: 'flex',
    marginTop: '1rem',
    marginBottom: '1rem',
    zIndex: 4,
  },
  appBar: {
    zIndex: 5,
  },
  title: {
    flexGrow: 1,
    color: 'white',
    cursor: 'pointer',
    textDecoration: 'none',
  },
}))

export const Navbar: React.FC<NavbarProps> = () => {
  const styles = useStyles()
  const { removeToken, token } = useApollo()

  useEffect(() => {
    console.log('issignedin', token)
  }, [])
  return (
    <>
      <AppBar className={styles.appBar} color="primary" position="static" >
        <Container maxWidth="lg" className={styles.root}>
          <RssFeedIcon fontSize="large" color="secondary"/>
          <Typography component={Link} to="/" color="textPrimary" className={styles.title} variant="h4">RSS Aggregator</Typography>
          {token ? <Button onClick={() => { removeToken() }} color="secondary">Deconnexion</Button> : (
            <>
              <Button component={Link} to="/login" color="secondary">Connexion</Button>
              <Button component={Link} to="/register" variant="outlined" color="secondary">Créer un compte</Button>
            </>
          )}

        </Container>
      </AppBar>
    </>
  )
}
