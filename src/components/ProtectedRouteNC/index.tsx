import React, { FunctionComponent } from 'react'
import { useApollo } from '../../contexts/ApolloContext'
import {
  Route,
  Redirect,
  RouteProps,
  RouteComponentProps,
} from 'react-router-dom'

interface PrivateRouteProps extends RouteProps {
  component:
    | React.ComponentType<RouteComponentProps<any>>
    | React.ComponentType<any>
}

const PrivateRoute: FunctionComponent<PrivateRouteProps> = ({
  component: Component,
  ...rest
}) => {
  const { token } = useApollo()
  return (
    <Route
      {...rest}
      render={props => (!token ? ( // put your authenticate logic here
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: '/dashboard',
          }}
        />
      ))}
    />
  )
}

export default PrivateRoute
