import parse from 'html-react-parser'
import React, { useState } from 'react'
import { Link, Redirect } from 'react-router-dom'

import {
  Button, Card, CardActionArea, CardContent, CardHeader, CardMedia, makeStyles, Typography,
} from '@material-ui/core'
import Chip from '@material-ui/core/Chip'
import { red } from '@material-ui/core/colors'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'

import { useDeleteFeedMutation, useMarkArticlesReadMutation } from '../../generated/hooks'
import { FeedFragment } from '../../generated/types'
import CustomModal from '../Modal'

interface FeedProps {
feed: FeedFragment | undefined
refresh: ()=> void
}

const useStyles = makeStyles({
  root: {
    '&  *': {
      marginTop: '1rem',
    },

    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#348AA7',

      },
      '& input': {
        color: 'white',
      },
    },
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
  },
  avatar: {
    backgroundColor: red[500],
  },
  title: {
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  body: {

  },
})

const Feed: React.FC<FeedProps> = ({ feed, refresh }) => {
  const style = useStyles()
  const [deleteFeed] = useDeleteFeedMutation()
  const [markArticleRead] = useMarkArticlesReadMutation()
  const [openModal, setOpenModal] = useState<boolean>(false)
  const [openModalArticle, setOpenModalArticle] = useState<boolean>(false)
  const [modalData, setModalData] = useState < { title: string; author: string; content: string; link: string } | null >(null)

  const handleClose = () => {
    setOpenModal(false)
  }

  const handleCloseArticle = () => {
    setOpenModalArticle(false)
  }

  const deleteFeedFC = () => {
    deleteFeed({ variables: { feedId: feed!.id } })
    setOpenModal(false)
    refresh()
  }

  return (
    <div>
      {feed ? (
        <>
          <CustomModal
            isOpen={openModalArticle}
            handleClose={handleCloseArticle}
            title={modalData?.title || 'Pas de titre'}
            subtitle={modalData?.author || "Pas d'auteur"}
          >
            <Button onClick={() => { window.location.href = modalData!.link || '' }}>Lien vers l'article du site</Button>
            {modalData ? <Typography>{ parse(modalData?.content as string) || 'Pas de contenu'}</Typography> : 'Pas de contenu'}
          </CustomModal>
          <div className={style.root}>
            <div className={style.title}>
              <Typography variant="h4">{feed.name}</Typography>
              <DeleteForeverIcon onClick={() => setOpenModal(true)} style={{ cursor: 'pointer' }} color="error"/>
            </div>
            <div className={style.body}>
              <CustomModal
                isOpen={openModal}
                handleClose={handleClose}
                title="Etes vous sur de vouloir supprimer ce feed ?"
                subtitle="Etes vous sur de vouloir supprimer ce feed ?"
              >

                <Button variant="outlined" onClick={deleteFeedFC} color="primary">Supprimer le feed</Button>
              </CustomModal>
              {feed.websites.length > 0 ? (
                <>
                  {feed.websites.map(website => {
                    return (
                      <div>
                        {website.articles?.map(article => {
                          return (
                            <Card onClick={() => {
                              markArticleRead({ variables: { articleLink: article.link, websiteId: website.id } })
                              setModalData({ author: article.author || '', title: article.title, content: article.content, link: article.link })
                              setOpenModalArticle(true)
                              // window.location.href = article.link
                            }}
                            >
                              {article.read && <CheckCircleIcon fontSize="large" style={{ color: 'green' }}/>}
                              <CardActionArea>
                                <CardHeader
                                  avatar={(
                                    <Chip aria-label="recipe" label={website.title} />)}
                                  title={article.author}
                                  subheader={article.pubDate}
                                />
                                <CardContent>
                                  <Typography gutterBottom variant="h5" component="h2">
                                    {article.title}
                                  </Typography>
                                  <Typography variant="body2" color="textSecondary" component="p">{article.summary}</Typography>
                                </CardContent>
                              </CardActionArea>
                            </Card>
                          )
                        })}
                      </div>
                    )
                  })}
                </>
              ) : (
                <>
                  <img src="/images/article.svg" alt="ok" />
                  <Typography variant="h4">Quelles sources voulez vous suivre ?</Typography>
                  <Typography variant="body1">Vous pouvez suivre des sites et des feeds</Typography>

                </>
              )}

            </div>
          </div>
        </>
      ) : (
        <>
          Clicker sur un feed
        </>
      )}
    </div>
  )
}

export default Feed
