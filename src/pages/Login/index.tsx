import React, { useEffect, useState } from 'react'
import { useApollo } from '../../contexts/ApolloContext'
import { Button, CircularProgress, Container, makeStyles, Paper, Snackbar, TextField, Typography } from '@material-ui/core'
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert'
import { blue } from '@material-ui/core/colors'

import { useSignInMutation } from '../../generated/hooks'

interface LoginProps {

}

function Alert(props: AlertProps) {
  return <MuiAlert elevation={6} variant="filled" {...props} />
}

const useStyles = makeStyles({
  root: {
    marginTop: '3rem',
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#348AA7',

      },
      '& input': {
        color: 'white',
      },
    },
  },
  field: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    '&> *': {
      marginTop: '2rem',
    },
  },
  validateButton: {
    marginTop: '2rem',
  },
  fabProgress: {
    display: 'flex',
    color: blue[500],
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  bott: {
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
})

const Login: React.FC<LoginProps> = () => {
  const style = useStyles()
  const [email, setEmail] = useState('')
  const { setToken } = useApollo()
  const [password, setPassword] = useState('')
  const [error, setError] = useState('')
  const [loading, setLoading] = useState(false)
  const [open, setOpen] = useState(false)
  const [signIn] = useSignInMutation()

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway')
      return

    setOpen(false)
  }

  const onSubmit = async () => {
    setLoading(true)
    if (!email || !password) {
      setError('Email and password must be filled')
      setOpen(true)
      setLoading(false)
      return
    }
    try {
      const res = await signIn({ variables: { email, password } })

      setToken(res?.data?.authenticateUserWithPassword?.token as string)
      setLoading(false)
    } catch (err) {
      setOpen(true)
      setLoading(false)
      setError(err.message)
    }
  }

  return (
    <Container maxWidth="lg" className={style.root} >
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          {error}
        </Alert>
      </Snackbar>

      <Typography style={{ color: '#ffffff' }} variant="h4">Se connecter</Typography>
      <div className={style.field}>
        <TextField required label="Email" variant="outlined" onChange={e => setEmail(e.target.value)} />
        <TextField required label="Mot de passe" variant="outlined" type="password" onChange={e => setPassword(e.target.value)} />
      </div>
      <div className={style.bott}>
        <Button className={style.validateButton} variant="outlined" color="secondary" onClick={onSubmit}>Connexion</Button>
        {loading && <CircularProgress size={30} className={style.fabProgress} />}
      </div>

    </Container>
  )
}

export default Login
