import React, { useState } from 'react'
import { Button, Container, makeStyles, TextField, Typography } from '@material-ui/core'
import Snackbar from '@material-ui/core/Snackbar'
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert'
import { useSignUpMutation } from '../../generated/hooks'

interface RegisterProps {

}

const useStyles = makeStyles({
  root: {
    marginTop: '3rem',
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#ffffff',
      },
      '& input': {
        color: '#ffffff',
      },
    },
  },
  field: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    '&> *': {
      marginTop: '2rem',
    },
  },
  validateButton: {
    marginTop: '2rem',
  },

})

function Alert(props: AlertProps) {
  return <MuiAlert elevation={6} variant="filled" {...props} />
}

const Register: React.FC<RegisterProps> = () => {
  const style = useStyles()
  const [signUp] = useSignUpMutation()
  const [email, setEmail] = useState('')
  const [error, setError] = useState('')
  const [firstname, setFirstname] = useState('')
  const [lastname, setLastname] = useState('')
  const [password, setPassword] = useState('')
  const [open, setOpen] = React.useState(false)

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway')
      return

    setOpen(false)
  }

  const createAccount = async () => {
    try {
      const res = await signUp({ variables: { email, lastname, password, firstname } })
      window.location.href = '/login'
    } catch (err) {
      console.log(err)
      setError(err.message)
      setOpen(true)
    }
  }
  const handleClick = () => {
    setOpen(true)
  }

  return (
    <Container maxWidth="lg" className={style.root} >
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          {error}
        </Alert>
      </Snackbar>
      <Typography color="primary" variant="h4">Créer un compte</Typography>
      <div className={style.field}>
        <TextField required label="Prénom" onChange={e => setFirstname(e.target.value)} variant="outlined" />
        <TextField required label="Nom" onChange={e => setLastname(e.target.value)} variant="outlined" />
        <TextField required label="Email" variant="outlined" onChange={e => setEmail(e.target.value)} />
        <TextField type="password" required label="Mot de passe" variant="outlined" onChange={e => setPassword(e.target.value)}/>
      </div>
      <Button className={style.validateButton} onClick={() => createAccount()} variant="outlined" color="secondary">Créer le compte</Button>
    </Container>
  )
}
export default Register
