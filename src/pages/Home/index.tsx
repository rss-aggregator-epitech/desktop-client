import React from 'react'

import { makeStyles, Paper, Typography } from '@material-ui/core'

interface HomeProps {

}

const useStyles = makeStyles({
  paper: {
    marginTop: '2rem',
    padding: '2rem',
  },
  createAccountButton: {
    marginTop: '2rem',
    float: 'right',
  },
})

const Home: React.FC<HomeProps> = () => {
  const style = useStyles()
  return (
    <div>
      <Paper className={style.paper}>
        <Typography color="textSecondary" variant="h4">
          Qu'est-ce que RSS?
        </Typography>
        <Typography color="textPrimary" variant="body1">
          RSS est l'abréviation de Really Simple Syndication. Il s'agit de fichiers facilement lisibles par un ordinateur, appelés fichiers XML, qui mettent automatiquement à jour les informations.
          Ces informations sont récupérées par le lecteur de flux RSS d'un utilisateur qui convertit les fichiers et les dernières mises à jour des sites Web dans un format facile à lire. Un flux RSS reprend les titres, les résumés et les avis de mise à jour, puis renvoie aux articles de la page de votre site Web préféré.
          Ce contenu est distribué en temps réel, de sorte que les premiers résultats du flux RSS sont toujours le dernier contenu publié pour un site web.
          Un flux RSS vous permet de créer votre propre eZine personnalisé avec le contenu le plus récent sur les sujets et les sites Web qui vous intéressent.
        </Typography>
      </Paper>
      <Paper className={style.paper}>
        <Typography color="textSecondary" variant="h4">
          Comment l'information passe-t-elle du site Web à votre flux ?
        </Typography>
        <Typography color="textPrimary" variant="body1">
          L'auteur de votre site Web ou podcast préféré crée un flux RSS qui tient à jour une liste de nouvelles mises à jour ou notifications. Vous pouvez consulter cette liste de votre propre chef ou vous abonner au flux pour que les mises à jour s'affichent dans votre propre lecteur de flux. Vous êtes ainsi immédiatement informé des mises à jour.
        </Typography>
      </Paper>
      <Paper className={style.paper}>
        <Typography color="textSecondary" variant="h4">
          RSS Aggregator
        </Typography>
        <Typography color="textPrimary" variant="body1">
          Alors, comment cela fonctionne-t-il vraiment ? Un agrégateur est responsable de la commodité des flux RSS.

          L'agrégateur RSS vérifie automatiquement les nouveaux contenus sur les sites Web. Il transfère immédiatement ce contenu dans votre lecteur de flux, ce qui vous évite d'avoir à vérifier chaque site Web individuellement pour trouver du nouveau contenu.

          L'agrégateur garde même une trace de ce que vous avez lu et de ce que vous n'avez pas lu en répertoriant le nombre d'articles ou de morceaux de contenu pour chaque site Web que vous suivez et qui n'ont pas été vus. Cela vous permet de parcourir rapidement le contenu des sites Web qui vous intéressent.
        </Typography>
      </Paper>
    </div>
  )
}
export default Home
