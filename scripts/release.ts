import axios from 'axios'

(async () => {
let code = 0

try {
  // @ts-ignore
  const [_, __, endpoint, version, type, branch] = process.argv
  const authorization = process.env.SECRET

  if (!authorization) throw new Error('Missing CI secret')
  if (!endpoint) throw new Error('Missing endpoint')
  if (!version) throw new Error('Missing version')
  if (!type || (type !== 'desktop_linux' && type !== 'desktop_macos')) throw new Error('Missing or invalid type')
  if (!branch || (branch !== 'master' && branch !== 'staging')) throw new Error('Missing or invalid branch name')

  const name = `RSSAggregator-${version}${type === 'desktop_macos' ? '-mac.zip' : '.AppImage'}`
  const url = `https://gitlab.com/rss-aggregator-epitech/desktop-client/-/jobs/artifacts/${branch}/raw/release/${name}?job=build`

  const config = {
    headers: {
      authorization,
    },
  }

  await axios.post(`${endpoint}/admin/api`, {
    query: `
      mutation($type: ReleaseTypeType!, $version: String!, $url: String!) {
        updateReleaseURL(type: $type, version: $version, url: $url) 
      }
    `,
    variables: { type, version, url },
  }, config)

  console.log('done')
} catch (e) {
  code = 1
  console.error(e)
}

process.exit(code)
})()
